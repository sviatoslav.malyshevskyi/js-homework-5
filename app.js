'use strict';

const newUser = createNewUser();
const login = newUser.getLogin();
const age = newUser.getAge(newUser.birthday);
const password = newUser.getPassword();
console.log(login);
console.log(age);
console.log(password);

function createNewUser() {
    let dateString = prompt('Enter your date of birth in the following format: dd.mm.yyyy');

    const newUser = {
        getLogin() {
            return this.firstName[0].toLowerCase()
                + this.lastName.toLowerCase();
        },
        setFirstName() {
            return prompt('Enter your first name: ');
        },
        setLastName() {
            return prompt('Enter your last name: ');
        },

        birthday: dateString,
        getAge(dateString) {
            let today = new Date();
            const birthday = new Date(dateString);
            let age = today.getFullYear() - birthday.getFullYear();
            let month = today.getMonth() - birthday.getMonth();
            if (month < 0 || (month = 0 && today.getDate() < birthday.getDate())) {
                age--;
            }
            return age;
        },

        getPassword() {
            return login[0].toUpperCase()
                + login.slice(1, login.length)
                + dateString.slice(6, 10);
        }
    };

    Object.defineProperty(newUser, 'firstName', {
        get() {
            return newUser.setFirstName();
        }
    });

    Object.defineProperty(newUser, 'lastName', {
        get() {
            return newUser.setLastName();
        }
    });

    return newUser;
}